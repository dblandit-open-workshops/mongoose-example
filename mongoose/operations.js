const { executeOneManyOp } = require('../utils/helpers');

const mockMovie = {
    "plot":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultrices viverra dolor nec ultricies. Integer volutpat, turpis in faucibus pellentesque, leo enim malesuada nibh, dictum semper leo tellus sit amet.",
    "genres":["Drama","Fantasy"],
    "runtime":120,
    "rated":"UNRATED",
    "cast":["Matias","Leandro","Chiques del curso"],
    "num_mflix_comments":2,
    "poster":"https://www.dblandit.com/img/logo-cuadrado-01.png",
    "title":"OpenCourse 2020",
    "fullplot":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultrices viverra dolor nec ultricies. Integer volutpat, turpis in faucibus pellentesque, leo enim malesuada nibh, dictum semper leo tellus sit amet.",
    "languages":["Spanish"],
    "released":"2020-01-28T00:00:00.000Z",
    "directors":["Juan Zaffaroni"],
    "writers":["Matias", "Leandro"],
    "awards":{"wins":1,"nominations":0,"text":"1 win."},
    "lastupdated":"2020-01-28T03:27:45.437Z",
    "year":2020,
    "imdb":{"rating":9.0,"votes":448,"id":488},
    "countries":["Argentina"],
    "type":"movie",
    "tomatoes":{"viewer":{"rating":8.9,"numReviews":53,"meter":67},"lastUpdated":"2020-01-28T19:06:35.000Z"}
};

const insertOneOperation = (Model) => {
    const newMovie = new Model({
        title: "OpenCourse 2020",
        fullplot: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultrices viverra dolor nec ultricies. Integer volutpat, turpis in faucibus pellentesque, leo enim malesuada nibh, dictum semper leo tellus sit amet.",
        year: 2020,
        cast: ["Mati", "Lean", "Chiques del curso"],
        poster: "https://www.dblandit.com/img/logo-cuadrado-01.png",
        released: new Date("2020-01-28T00:00:00.000Z"),
        awards: {"wins":1,"nominations":0,"text":"1 win."}
    });

    return newMovie.save()
};

const insertManyOperation = (Model) => {
    return Model.insertMany([{...mockMovie}, {...mockMovie}, {...mockMovie}])
};

const readOneOperation = (Model) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return Model.findOne({title: process.argv[4]})
};

const readManyOperation = (Model) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return Model.find({title: process.argv[4]})
};

const updateOneOperation = (Model) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de actualizacion del campo title")
    }
    return Model.findOne({title: "OpenCourse 2020"})
        .then(movie => {
            movie.title = process.argv[4];
            return movie.save()
        })
};

const updateManyOperation = (Model) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de actualizacion del campo title")
    }
    return Model.updateMany({title: "OpenCourse 2020"}, {$set: {title: process.argv[4]}})
};

const deleteOneOperation = (Model) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return Model.deleteOne({title: process.argv[4]})
};

const deleteManyOperation = (Model) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return Model.deleteMany({title: process.argv[4]})
};

const aggregateOperation = (Model) => {
    return Model.aggregate()
        .match({genres: "Documentary"})
        .group({_id: {$year: "$released"}, puntajeTotalAnio: {$avg: "$imdb.rating"}})
        .addFields({anio: "$_id"})
        .project({_id: 0})
        .sort({puntajeTotalAnio: -1})
};

module.exports = {
    create: executeOneManyOp(insertOneOperation, insertManyOperation),
    read: executeOneManyOp(readOneOperation, readManyOperation),
    update: executeOneManyOp(updateOneOperation, updateManyOperation),
    delete: executeOneManyOp(deleteOneOperation, deleteManyOperation),
    aggregate: executeOneManyOp(aggregateOperation, undefined)
};