const mongoose = require('mongoose');
const uri = "mongodb://localhost:27017/movies";

const logger = require('../utils/logger')(__filename);
const operations = require('./operations');

const Movie = require('./models/Movie');

mongoose.connect(uri, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {
        logger.info("Conexion exitosa a la db movies");

        operations[process.argv[2]](mongoose.connection, Movie)

    })
    .catch(err => {
        logger.error(err);
        process.exit(1)
    });