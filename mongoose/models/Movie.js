const mongoose = require('mongoose');

const Movie = new mongoose.Schema({
    title: String,
    fullplot: { type: String },
    year: Number,
    cast: [String],
    poster: { type: String },
    released: { type: Date },
    awards: { type: {
        nominations: Number,
        text: String,
        wins: Number
    }}
});

module.exports = mongoose.model('Movie', Movie);