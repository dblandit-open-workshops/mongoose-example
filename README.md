# Mongoose-example
Repositorio dedicado a la demostración de las diferencias que existen entre el driver de [MongoDB](https://mongodb.github.io/node-mongodb-native/3.5/api/) y [Mongoose](https://mongoosejs.com/) 
para el manejo de operaciones [CRUD](https://es.wikipedia.org/wiki/CRUD) o ABM.

### Requisitos
- NodeJS (última versión estable)
- MongoDB

### Primeros pasos
1. Clonar el repositorio con el mecanismo de su preferencia (ssh o `git clone`).
2. Tipear en una terminal dentro del directorio que contiene el repositorio `npm install`.
3. Asegurarse de que existe levantada una instancia del server de mongo en el puerto `27017`. (Ejecutable llamado `mongod`).
4. Asegurarse de que la colección `movies` se encuentra en la base de datos `movies` (si no es así revisar clases anteriores para importar conn la herramienta `mongoimport`).

#### Funcionamiento
Las operaciones CRUD disponibles pueden ser realizadas tanto para un documento como para muchos, excepto para el script de Aggregation Framework y se disponen de la siguiente manera:

- #### Create
    Inserta uno ó varios documentos mocks para la colección movies.
    - Driver MongoDB: `npm run mongodb-create [one ó many]`
    - Mongoose: `npm run mongoose-create [one ó many]`
- #### Read
    Devuelve uno ó varios documentos que matcheen con el título de la película pasada por parámetro (Si el título contiene espacios escribirlo entre comillas dobles. Por ej `"OpenCourse 2020"`).
    - Driver MongoDB: `npm run mongodb-read [one ó many] [titulo de una pelicula]`
    - Mongoose: `npm run mongoose-read [one ó many] [titulo de una pelicula]`
- #### Update
    Cambia el título de la película `OpenCourse 2020` a uno ó varios al nuevo título pasado como parámetro.
    - Driver MongoDB: `npm run mongodb-update [one ó many] [nuevo título]`
    - Mongoose: `npm run mongoose-update [one ó many] [nuevo título]`
- #### Delete
    Elimina uno ó varios documentos que matcheen con el título de la película pasada por parámetro.
    - Driver MongoDB: `npm run mongodb-delete [one ó many] [titulo de una pelicula]`
    - Mongoose: `npm run mongoose-delete [one ó many] [titulo de una pelicula]`    
- #### Aggregation
    Realiza una operación "aggregation" predetermina y retorna el resultado por pantalla. 
    - Driver MongoDB: `npm run mongodb-aggregate`
    - Mongoose: `npm run mongoose-aggregate`
