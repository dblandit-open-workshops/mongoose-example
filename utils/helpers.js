const logger = require('./logger')(__filename);

const checkForValidArguments = (connection) => {
    if (!process.argv[3]) {
        logger.error("Se debe ingresar una opcion como parametro -> one ó many");
        connection.close()
            .then(() => {
                process.exit(1);
            });
    }
};

const executeOneManyOp =  (oneOperation, manyOperation) => (connection, entity) => {
    const operation = {
        one: oneOperation,
        many: manyOperation
    };

    checkForValidArguments(connection);

    if (!operation[process.argv[3]]) {
        logger.error("El comando ingresado no es válido");
        connection.close()
            .then(() => {
                process.exit(1);
            })
    } else {
        operation[process.argv[3]](entity)
            .then(result => {
                logger.info(`Resultado: \n ${JSON.stringify(result, null, 2)}`);
                connection.close()
                    .then(() => {
                        process.exit(0)
                    })
            })
            .catch(err => {
                logger.error(err);
                connection.close()
                    .then(() => {
                        process.exit(1)
                    });
            });
    }
};

module.exports = { checkForValidArguments, executeOneManyOp };