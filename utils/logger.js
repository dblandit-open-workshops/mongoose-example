const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf, label } = format;

const path = require('path');

const logFormat = printf(({ level, message, timestamp, label }) => {
    return `${timestamp} ${level} [${label}]: ${message}`;
});

/**
 * Returns winston logger to log through the project
 * @param caller {String} - call with  __filename as parameter to know where log occur
 * @returns {winston.Logger} - winston logger
 */
const logger = (caller) =>
{
    return createLogger({
        format: combine(label({ label: path.basename(caller) }), timestamp(), logFormat),
        transports: [
            new transports.Console()
        ]
    });
};

module.exports = logger;
