const { executeOneManyOp } = require('../utils/helpers');

const newMovie = {
    "plot":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultrices viverra dolor nec ultricies. Integer volutpat, turpis in faucibus pellentesque, leo enim malesuada nibh, dictum semper leo tellus sit amet.",
    "genres":["Drama","Fantasy"],
    "runtime":120,
    "rated":"UNRATED",
    "cast":["Matias","Leandro","Chiques del curso"],
    "num_mflix_comments":2,
    "poster":"https://www.dblandit.com/img/logo-cuadrado-01.png",
    "title":"OpenCourse 2020",
    "fullplot":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultrices viverra dolor nec ultricies. Integer volutpat, turpis in faucibus pellentesque, leo enim malesuada nibh, dictum semper leo tellus sit amet.",
    "languages":["Spanish"],
    "released":"2020-01-28T00:00:00.000Z",
    "directors":["Juan Zaffaroni"],
    "writers":["Matias", "Leandro"],
    "awards":{"wins":1,"nominations":0,"text":"1 win."},
    "lastupdated":"2020-01-28T03:27:45.437Z",
    "year":2020,
    "imdb":{"rating":9.0,"votes":448,"id":488},
    "countries":["Argentina"],
    "type":"movie",
    "tomatoes":{"viewer":{"rating":8.9,"numReviews":53,"meter":67},"lastUpdated":"2020-01-28T19:06:35.000Z"}
};

const insertOneOperation = (coll) => {
    return coll.insertOne(newMovie)
};

const insertManyOperation = (coll) => {
    return coll.insertMany([{...newMovie}, {...newMovie}, {...newMovie}])
};

const readOneOperation = (coll) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return coll.findOne({title: process.argv[4]})
};

const readManyOperation = (coll) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return coll.find({title: process.argv[4]}).limit(3).toArray()
};

const updateOneOperation = (coll) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de actualizacion del campo title")
    }
    return coll.updateOne({title: 'OpenCourse 2020'}, {$set: {title: process.argv[4]}})
};

const updateManyOperation = (coll) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de actualizacion del campo title")
    }
    return coll.updateMany({title: 'OpenCourse 2020'}, {$set: {title: process.argv[4]}})
};

const deleteOneOperation = (coll) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return coll.deleteOne({title: process.argv[4]})
};

const deleteManyOperation = (coll) => {
    if (!process.argv[4]) {
        return Promise.reject("Falta un parametro de filtro para el campo title")
    }
    return coll.deleteMany({title: process.argv[4]})
};

const aggregateOperation = (coll) => {
    return coll.aggregate([
        {$match: {genres: "Documentary"}},
        {$group: {_id: {$year: "$released"}, puntajeTotalAnio: {$avg: "$imdb.rating"}}},
        {$addFields: {anio: "$_id"}},
        {$project: {_id: 0}},
        {$sort: {puntajeTotalAnio: -1}}
    ]).toArray()
};

module.exports = {
    create: executeOneManyOp(insertOneOperation, insertManyOperation),
    update: executeOneManyOp(updateOneOperation, updateManyOperation),
    read: executeOneManyOp(readOneOperation, readManyOperation),
    delete: executeOneManyOp(deleteOneOperation, deleteManyOperation),
    aggregate: executeOneManyOp(aggregateOperation, undefined)
};