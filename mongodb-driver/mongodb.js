const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb://localhost:27017";

const logger = require('../utils/logger')(__filename);
const operations = require('./operations');

const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

client.connect()
    .then(client => {
        const db = client.db('movies');
        logger.info("Conexión exitosa a la db movies");

        operations[process.argv[2]](client, db.collection('movies'));

    })
    .catch(err => {
        logger.error(err);
        process.exit(1)
    });
